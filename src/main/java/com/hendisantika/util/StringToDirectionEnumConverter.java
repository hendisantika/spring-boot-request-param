package com.hendisantika.util;

import com.hendisantika.model.Direction;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-request-param
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/6/22
 * Time: 12:04
 * To change this template use File | Settings | File Templates.
 */
@Component
public class StringToDirectionEnumConverter implements Converter<String, Direction> {

    @Override
    public Direction convert(String source) {
        try {
            return source.isEmpty() ? null : Direction.valueOf(source.trim().toUpperCase(Locale.ROOT));
        } catch (Exception e) {
            return Direction.UNKNOWN; //
        }
    }
}
