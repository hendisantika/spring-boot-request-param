package com.hendisantika.model;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-request-param
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/6/22
 * Time: 12:03
 * To change this template use File | Settings | File Templates.
 */
public enum DefaultEnum {
    HIDE, PLAY, SEEK, PAUSE
}
