package com.hendisantika.controller;

import com.hendisantika.model.Feedback;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-request-param
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/6/22
 * Time: 12:06
 * To change this template use File | Settings | File Templates.
 */
@RestController
@Slf4j
public class MultipleParamsController {

    /*@RequestMapping(value = "/feedback", method = RequestMethod.POST)
    public String getUserFeedback(@RequestParam Map<String,String> requestParams) throws Exception{
        String email=requestParams.get("email");
        String username=requestParams.get("username");
        String feedback=requestParams.get("feedback");
        System.out.println("Email : " + email +
                "\n Username :" + username +
                "\n feedback : "+ feedback);
        // your code logic
        return "feedback";
    }*/
   /* @RequestMapping(value = "/feedback", method = RequestMethod.POST)
    public String getFeedback(@RequestParam("email") String email,
                              @RequestParam("username") String username,
                              @RequestParam("title") String title,
                              @RequestParam("feedback") String feedback) throws Exception{
        System.out.println("Email : " + email +
                "\n Username :" + username +
                "\n feedback : "+ feedback +
                "\n title : " + title);
        // your code logic
        return "feedback";
    }*/
    @RequestMapping(value = "/feedback", method = RequestMethod.POST)
    public String getFeedback(Feedback feedback) throws Exception {
        log.info("Email : " + feedback.getEmail() +
                "\n Username :" + feedback.getUsername() +
                "\n feedback : " + feedback.getFeedback() +
                "\n title : " + feedback.getTitle());
        // your code logic
        return "feedback";
    }

    @GetMapping("/orderStatus")
    public String getOrderStatus(@RequestParam(name = "orderId", required = false, defaultValue = "999") Integer orderId) {
        log.info("Order orderId " + orderId);
        return "Order orderId: " + orderId;
    }

    @DeleteMapping("/deleteOrder/{orderId}")
    public ResponseEntity<Long> deleteOrder(@PathVariable(value = "orderId") Long orderId) {
        // Access the DB and delete the order
        return ResponseEntity.ok(orderId);
    }

    @PostMapping(value = "/users")
    public String getUserList(@RequestParam("user") List<String> users) throws Exception {
        log.info("User List : " + users);
        // your code logic
        return "users";
    }

    @PostMapping(value = "/feedbacksOfUsers")
    public String feedbacksOfUsers(@RequestParam MultiValueMap<String, String> requestParams) throws Exception {
        log.info("Multi Value Map : " + requestParams);
        // your code logic
        return "users";
    }
}
