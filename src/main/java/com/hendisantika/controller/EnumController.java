package com.hendisantika.controller;

import com.hendisantika.model.DefaultEnum;
import com.hendisantika.model.Direction;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-request-param
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/6/22
 * Time: 12:09
 * To change this template use File | Settings | File Templates.
 */
@RestController
@Tag(name = "/plotGraph", description = "Endpoint for list all books")
@Slf4j
public class EnumController {
    @GetMapping(path = "/plotGraph")
    @Operation(
            summary = "Add plotGraph",
            description = "Add plotGraph."
    )
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            String.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Not Found",
                    responseCode = "404",
                    content = @Content
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Internal Error",
                    responseCode = "404",
                    content = @Content
            )
    })
    public String plotGraph(@RequestParam("direction") Direction direction) {
        log.info("Plot Graph : " + direction);
        return "Plot Graph : " + direction;
    }

    @GetMapping(path = "/defaultConverter")
    @Operation(
            summary = "Add defaultConverter",
            description = "Add defaultConverter."
    )
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            String.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Not Found",
                    responseCode = "404",
                    content = @Content
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Internal Error",
                    responseCode = "404",
                    content = @Content
            )
    })
    public String defaultConverter(@RequestParam("default") DefaultEnum defaultEnum) {
        log.info("Default Converter : " + defaultEnum);
        return "Default Converter : " + defaultEnum;
    }
}
