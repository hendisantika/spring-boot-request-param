# Spring Boot Request Param

### Things todo list:

1. Clone this repository: `git clone https://gitlab.com/hendisantika/spring-boot-request-param.git`
2. Navigate to the folder: `cd spring-boot-request-param`
3. Run the application: `mvn clean spring-boot:run`
4. Open your favorite browser: http://localhost:8080/swagger-ui 